import { Component } from "@angular/core";
import { Platform } from "ionic-angular";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Md5 } from "ts-md5/dist/md5";
import { Shake } from "@ionic-native/shake";
import { Vibration } from "@ionic-native/vibration";
import { AlertController } from "ionic-angular";
import * as SockJS from "sockjs-client";
import webstomp from "webstomp-client";
import {
  DeviceMotion,
  DeviceMotionAccelerationData,
} from "@ionic-native/device-motion";

@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage {
  dataToken: Observable<any>;
  dataUrl: Observable<any>;
  account_id: string;

  webSocket: string;
  account: string;
  app_id: string;
  userManage: string;
  passwordManage: string;
  _ws: any;
  _client: any;
  browser: any;
  subscription: any;
  webAPI: string;

  constructor(
    private iab: InAppBrowser,
    private http: HttpClient,
    private shake: Shake,
    private platform: Platform,
    private vibration: Vibration,
    private alertCtrl: AlertController,
    private deviceMotion: DeviceMotion
  ) {
    this.account = "0969545721";
    this.app_id = "1";
    this.userManage = "ipi";
    this.passwordManage = "ipi@2021";
    this.webSocket = "http://gamelb.2nong.vn/";
    this.webAPI = "http://gamelb.2nong.vn";
    this.getTokenGame();
  }

  connectSocket() {
    var watch1;
    var watch2;
    var watch3;
    console.log(this.account_id);

    this._ws = new SockJS("http://gamelb.2nong.vn/gs-guide-websocket");
    this._client = webstomp.over(this._ws);
    this._client.connect(
      {},
      (msg) => {
        this._client.subscribe(
          "/" + this.account_id + "/onAction",
          (result) => {
            // console.log("resultonAction");
            console.log(result.body);
            const obj = JSON.parse(result.body);
            console.log(obj.data.action);
            switch (obj.data.action) {
              case "exitgame":
                this.browser.close();
                break;
              case "playgame":
                this.platform.ready().then(() => {
                  this.subscription = this.deviceMotion
                    .watchAcceleration({
                      frequency: 400,
                    })
                    .subscribe(
                      (acceleration: DeviceMotionAccelerationData) => {
                        let totalAcceleration = Math.abs(
                          acceleration.x + acceleration.y + acceleration.z
                        );
                        if (totalAcceleration > 120) {
                          this.vibration.vibrate(1000);
                          this.postShake(3);
                        } else if (totalAcceleration > 80) {
                          this.vibration.vibrate(1000);
                          this.postShake(2);
                        } else if (totalAcceleration > 40) {
                          this.vibration.vibrate(1000);
                          this.postShake(1);
                        }
                      },

                      (error: any) => console.log(error)
                    );
                });
                break;
              default:
                if (this.subscription) {
                  this.subscription.unsubscribe();
                }
                break;
            }
          }
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }

  queryParams(params) {
    return Object.keys(params)
      .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
      .join("&");
  }

  getTokenGame() {
    var d = new Date();
    var n = d.getTime();
    var url =
      this.webSocket +
      "/rest/getToken?username=" +
      this.userManage +
      "&password=" +
      this.passwordManage;
    this.dataToken = this.http.get(url);
    console.log(this.dataToken);

    this.dataToken.subscribe((data) => {
      if (data.data) {
        var stock = this.account + this.app_id + n + data.data.sercret;
        var md5 = Md5.hashStr(stock);
        this.getGameUrl(data.data.access_token, n, md5);
      }
    });
  }

  getGameUrl(token, time, md5) {
    const params = {
      account: this.account,
      app_id: this.app_id,
      token: token,
      time: time,
      md5: md5,
    };
    var query = this.queryParams(params);
    var url = this.webSocket + "/rest/getGameUrl";
    if (query) {
      url += "?" + query;
    }
    console.log(url);
    this.dataUrl = this.http.get(url);
    this.dataUrl.subscribe((data) => {
      console.log(data);
      this.account_id = data.data.account_id;
      this.connectSocket();
      this.openTab(data.data.url);
    });
  }

  postShake(shake) {
    const params = {
      amplitude: shake,
      account_id: this.account_id,
    };
    const url = this.webAPI + "/rest/doShake";
    this.dataUrl = this.http.post(url, JSON.stringify(params), {
      headers: { "Content-Type": "application/json; charset=utf-8" },
    });
    this.dataUrl.subscribe((data) => {
      console.log(data);
    });
  }

  openTab(url) {
    this.browser = this.iab.create(url, "_blank");
  }
}
